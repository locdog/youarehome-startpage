# YouareHome startpage

A [hack of m-hrstv/startpage](https://github.com/m-hrstv/startpage/tree/master)

<img src="https://gitlab.com/locdog/youarehome-startpage/-/raw/main/YouareHome startpage/Screenshot.png">

## Authors and acknowledgment
THis startpage is a hack of [m-hrstv's startpage hosted on gitlab](https://github.com/m-hrstv/startpage/tree/master).  I have merely changed the background and links. The [origional startpage is from reddit r/statrpages.](https://www.reddit.com/r/startpages/comments/psgn22/got_tired_of_nighttab_poked_around_a_page_i_made/)
## License
 The MIT License (MIT)

Copyright © 2021 <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## Project status
I merely modify links and images

